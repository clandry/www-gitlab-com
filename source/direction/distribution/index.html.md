---
layout: markdown_page
title: "Product Direction - Distribution"
---

- TOC
{:toc}

## Overview
GitLab is the engine that powers many software businesses. In a world where success requires developers to move quickly, it is
important to ensure that end users can accomplish their work as quickly as possible, with minimal interruptions, and they have access to the latest and greatest that GitLab has to offer. The Distribution group's mission is to set companies up for success by making it easy to deploy, maintain, and update a self-managed GitLab instance and ensure it is highly avaliable for their users. 

## Category

### Omnibus

Today we have a mature and easy to use Omnibus based build system, which is the
foundation for nearly all methods of deploying GitLab. It includes everything a
customer needs to run GitLab all in a single package, and is great for
installing on virtual machines or real hardware. We are committed to making our
package easier to work with, highly available, as well as offering automated
deployments on cloud providers like AWS.

### Cloud-native installation
We also want GitLab to be the best cloud native development tool, and offering a
great cloud native deployment is a key part of that. We are focused on offering
a flexible and scalable container based deployment on Kubernetes, by using
enterprise grade Helm charts.


The Helm charts are currently considered to be at the [Viable maturity level](https://about.gitlab.com/direction/maturity/). These epics will be used to define what it will take to get to the next maturity levels and track the work to be done:
[Viable to Complete](https://gitlab.com/groups/gitlab-org/-/epics/2260)
[Viable to Lovable](gitlab-org/charts/gitlab#1658)

## What's next

[Move to PostgreSQL 11 and 12](https://gitlab.com/groups/gitlab-org/-/epics/2184) 

PostgreSQL 11 and 12 included improvements to database sharding and partitioning. By moving to PostgreSQL 11 as the minimum required version of PostgreSQL, the Database group can make improvements to the performance and scalability of GitLab. The Distribution group is prioritizing the work to add support for PostgreSQL 11 and PostgreSQL 12, including extensive testing of GitLab upgrades with these PostgreSQL versions.

[Move the cloud-native install from Viable to Complete](https://gitlab.com/groups/gitlab-org/-/epics/2260)

The interest in moving applications to Kubernetes is growing. This includes moving self-hosted GitLab instances to Kubernetes. The recommended approach to deploying GitLab in Kubernetes is to use the GitLab Helm chart. The helm chart is not as mature as the Omnibus install method, but we continue to make great strides forward. The [Viable to Complete maturity epic](https://gitlab.com/groups/gitlab-org/-/epics/2260) is a top priority for the first half of 2020. It tracks the work to add the few GitLab features that are still missing, prove out the helm chart at large scale, and improve the documentation and configuration options. 

[Automate HA and Geo installations](https://gitlab.com/groups/gitlab-org/-/epics/1949)

It continues to be time consuming to deploy large-scale, self-managed instances of GitLab with a fully-distributed high availability architecture and Geo replication. We are committed to drastically reducing the time it takes for new users of GitLab to set up high availability and Geo. The [HA/Geo epic](https://gitlab.com/groups/gitlab-org/-/epics/1949) tracks the work to create automation tooling to eliminate many of the manual steps that are currently required.  
