---
layout: markdown_page
title: "Category Direction - Roadmaps"
---

- TOC
{:toc}

Last Reviewed: 2020-02-07

## Description

Large enterprises are continually working on increasingly more complex and larger
scope initiatives that cut across multiple teams and even departments, spanning months,
quarters, and even years. GitLab's vision is to provide robust, multi-level roadmaps that enable businesses to track current effort in flight, and plan upcoming work to best utilize their resources and focus on the right priorities.

GitLab will leverage [timeline-based roadmap visualizations](https://gitlab.com/gitlab-org/gitlab-ee/issues/7077) to help enterprises plan from small time scales (e.g. 2 week sprints for development 
teams) to large time scales (e.g. annual strategic initiatives for entire departments).
Enhanced issues and [issue boards](https://gitlab.com/groups/gitlab-org/-/epics/293)
[capabilities](https://gitlab.com/groups/gitlab-org/-/epics/383) will allow more
versatile and powerful backlog grooming and cross-sprint planning functionality.

GitLab will enable teams to **track execution** of these plans over time. In addition
to roadmap views as well as existing time tracking and story point estimation features
(called weights in GitLab), GitLab workflow management will also be improved with
[group-level customized workflows integrated into boards](https://gitlab.com/groups/gitlab-org/-/epics/424), as well as [burndown and burnup charts also integrated into boards](https://gitlab.com/groups/gitlab-org/-/epics/372).

## What's next & why

We've written a [mock press release](/direction/plan/portfolio_management/one_year_plan_portfolio_mgmt) describing where we intend to be by 2020-09-01. We will maintain this and update it as we sense and respond to our customers and the wider community.

To move towards our long term vision of robust roadmapping, we are working to provide a solid foundation of functionality that we can build on in the future. We are working to ensure that the roadmap view provides the information you need to understand current state of work, identify blockers or bottlenecks, and ensure upcoming work is prepared for.

To begin, we will be releasing multiple enhancements to the current Roadmap view:

- [Expand Epic on Roadmap to view Sub-Epics](https://gitlab.com/gitlab-org/gitlab/issues/7077)
- [Show Weight/Progress Information on Roadmap](https://gitlab.com/gitlab-org/gitlab/issues/5164)
- [Display Milestones on Roadmap](https://gitlab.com/gitlab-org/gitlab/issues/6802)

## Competitive landscape

<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

## Analyst landscape

<!-- What analysts and/or thought leaders in the space talking about, and how we stay
relevant from their perspective.-->

## Top Vision Item(s)

- [Enhance Roadmaps with Drag and Drop functionality](https://gitlab.com/gitlab-org/gitlab/issues/7725)
- [Provide multiple configurable Roadmaps](https://gitlab.com/gitlab-org/gitlab/issues/3640)
- [Surface Epic due Dates and Milestones on Roadmap](https://gitlab.com/gitlab-org/gitlab/issues/7071)
- [Show Dependencies and Critical Dependency Paths on Roadmap](https://gitlab.com/gitlab-org/gitlab/issues/33587)
